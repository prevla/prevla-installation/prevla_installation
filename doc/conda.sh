#!/bin/sh

echo --------------------------------- Conda Starts Ends -----------------------

echo Conda Instalisation starts

cd /home/inosens/texas
wget https://repo.anaconda.com/miniconda/Miniconda3-py310_23.1.0-1-Linux-x86_64.sh
chmod +x Miniconda3-py310_23.1.0-1-Linux-x86_64.sh
echo Mini conda basliyor
./Miniconda3-py310_23.1.0-1-Linux-x86_64.sh
echo Conda Instalisation ends
echo --------------------------------- Conda Ends-----------------------
sleep 5
echo
echo
echo
echo
echo
echo


echo --------------------------------- Env Creation Ends -----------------------


echo rawdar env starting
conda run -n base conda deactivate
conda create --name rawdar -y
echo rawdar env finished
echo mp env starting
conda create --name mp -y
conda run -n mp conda install pip
conda run -n mp pip install mediapipe ntplib 
echo mp env finished
echo gui env starting
conda create --name gui -y
conda run -n gui conda install pip
conda run -n gui pip install psutil ntplib 
echo gui env finished
echo --------------------------------- Env Creation Ends -----------------------
