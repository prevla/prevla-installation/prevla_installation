 
export PATH="$HOME/miniconda3/bin:$PATH"

conda remove -n gui --all -y
conda remove -n mp --all -y
conda remove -n rawdar --all -y

echo rawdar env starting
conda run -n base conda deactivate
conda create --name rawdar -y
echo rawdar env finished

echo mp env starting
conda create --name mp -y
conda run -n mp conda install pip
conda run -n mp pip install mediapipe ntplib pyodbc 
echo mp env finished

echo gui env starting
conda create --name gui -y
conda run -n gui conda install pip
conda run -n gui pip install psutil ntplib Flask pyodbc
echo gui env finished


echo server env starting
conda create --name server -y
conda run -n server conda install pip
conda run -n server pip install pyodbc opencv-python pandas psutil flask flask_cors sqlalchemy
echo server env finished

echo figure env starting
conda create --name figure -y
conda run -n figure conda install pip
conda run -n figure pip install pandas numpy matplotlib pyodbc pyqt5
echo figure env finished

echo      I  I  I  I  I  I  I  I  I  I  I      Env Creation Ends      I  I  I  I  I  I  I  I  I  I  I     
echo      I  I  I  I  I  I  I  I  I  I  I      Nodejs    
