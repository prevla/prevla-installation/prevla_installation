cd $HOME/Desktop/prevla/prevla_installation/installer/scripts

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm install v16.17.1
nvm use v16.17.1
																																																				
npm i @angular/cli@15.2.1
sudo npm install -g @angular/cli

cd $HOME/Desktop/prevla/prevlaui

npm install
npm install -g npm@9.6.4
