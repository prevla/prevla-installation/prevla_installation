cd $HOME/Desktop/prevla/prevla_installation/programs

wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main
sudo apt install -y google-chrome-stable
echo Chrome is OK

wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | sudo apt-key add -
echo "deb http://deb.anydesk.com/ all main" | sudo tee /etc/apt/sources.list.d/anydesk-stable.list
sudo apt install -y anydesk
echo Anydesk is OK

unzip radar_toolbox_1_00_00_26.zip
echo radar_toolbox is OK

chmod +x ticloudagent.run
./ticloudagent.run 
echo ticloudagent is OK

chmod +x mmwave_sdk_03_06_00_00-LTS-Linux-x86-Install.bin
sudo ./mmwave_sdk_03_06_00_00-LTS-Linux-x86-Install.bin
echo mmwave_sdk is OK